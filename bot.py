    # bot.py
import os
import subprocess
import random

#discord.py/examples
import discord
import asyncio
import time

import sqlite3
conn = sqlite3.connect('request.db')
cur = conn.cursor()

from discord.ext import commands
from dotenv import load_dotenv



def runcmd(command,timeout=250):
    ret = subprocess.run(command,shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE,encoding="utf-8",timeout=timeout)
    return ret


load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
pathapg='/root/apgmera/'
hauldomain='https://catagolue.hatsya.com/haul/'

validsyms=["1x256", "2x128", "4x64", "8x32", "C1",
"C2_4", "C2_2", "C2_1", "C4_4", "C4_1",
"D2_+2", "D2_+1", "D2_x", "D4_+4", "D4_+2", "D4_+1", "D4_x4", "D4_x1", "D8_4", "D8_1", ]


nicknames = {'skilife': 'b3-ks23-i', 'pedestrianlife': 'b38s23', 'honeylife': 'b38s238'
 ,'highlife':'b36s23'
,'dayandnight':'b3678s34678'
,'tlife':'b3s2-i34q'
,'vessellife':'b2n3-q5y6cs23-k'
,'leaplife':'b2n3s23-q'
,'life':'b3s23'
,'drylife':'b37s23'
,'x3vi':'b2c3aei4ajnr5acns2-ci3-ck4in5jkq6c7c'
,'novalox':'b2e3-k8s2-e3-cqy4qt8'
,'2x2':'b36s125'
 }

primes=[307 , 311 , 313 , 317 , 331 , 337 , 347 , 349 , 353 , 359 , 367 , 373 , 379 , 383 , 389 , 397 , 401 , 409 , 419 , 421 , 431 , 433 , 439 , 443 , 449 , 457 , 461 , 463 , 467 , 479 , 487 , 491 , 499 , 503 , 509 , 521 , 523 , 541 , 547 , 557 , 563 , 569 , 571 , 577 , 587 , 593 , 599 , 601 , 607 , 613 , 617 , 619 , 631 , 641 , 643 , 647 , 653 , 659 , 661 , 673 , 677 , 683 , 691 , 701 , 709 , 719 , 727 , 733 , 739 , 743 , 751 , 757 , 761 , 769 , 773 , 787 , 797 , 809 , 811 , 821 , 823 , 827 , 829 , 839 , 853 , 857 , 859 , 863 , 877 , 881 , 883 , 887 , 907 , 911 , 919 , 929 , 937 , 941 , 947 , 953]

def to_lower(argument):
    return argument.lower()
def to_upper(argument):
    return argument.upper()

def writedb(author,t,rule,symmetry):
    #
    return 'added'



# Change only the no_category default string
help_command = commands.DefaultHelpCommand(
    no_category = 'All Features'
)

# Create the bot and pass it the modified help_command


class Bot(commands.Bot):
    def __init__(self, *args, **kwargs):
        self.first_time = True
        self.owner = None
        self.assets_chn = None
        super().__init__(*args, **kwargs)

bot = Bot(
    command_prefix='./',
    description='A apgsearch front-end for Conwaylife Lounge',
    help_command = help_command
)


@bot.check
async def ignore_dms(ctx):
    return ctx.guild is not None



@bot.event
async def on_ready():
    print(f'{bot.user.name} has connected to Discord!')

@bot.command(name='request', help="'./request 20h b3s23 D2_x' for 20h searching on certain rules ||you have total 200h to spend.||")    
async def request(ctx,cpu_hours='20h',rule:to_lower='b3s23',symmetry='C1'):
    if ';' in rule: 
        err='Err, I don\'t like this argument.```{}```'.format(rule)
    else:
        err=''
    if '/'in rule:
        rule=rule.replace('/','')
    if rule in nicknames:
        rule=nicknames[rule]   

    response=''
    t=cpu_hours.strip('h')
    if t.isdigit()  :
        t=int(t)
        if t>=20 and t<=200:
            if symmetry in validsyms:
                #
                prime_num=random.choice(primes)
                db_return=writedb(ctx.author,t,rule,symmetry)#ctx.author.id
                if err=='':
                    response='OK, {}.Search **{}** in **{}** for {} cpu hours. The suffix of your hauls would be {}'.format(ctx.author.mention,rule,symmetry,t,prime_num)
                    print("./recompile.sh --rule {0} --symmetry {1} --profile; mv apgluxe {0}_{1};nice -n 0 ./{0}_{1} -v 0 -k search -p 6 -n 70000{2} -i 40 ;".format(rule,symmetry,prime_num))
            else:
                err+='"{}" is not a valid symmetry. '.format(symmetry)
        else:
            err+='"{}" does not match the requirement: ``` 20<=cpu_hours<=200. ```'.format(t)
    else:
        err+='"{}" is not a interger. '.format(t)        
    
    await ctx.send(err+response)
        






@bot.command(name='apgluxe', help="'./apgluxe life C1' | with non-input, it would search Life-C1 with haul size 500000.")
async def nine_nine(ctx,rule:to_lower='b3s23',symmetry='C1'):

    #
    if ';' in rule :
        rule='skilife'
    elif '/'in rule:
        rule=rule.replace('/','')
    if ';' in symmetry : 
        symmetry='random'   

        

    lol_keywords=['random','randomly','whatever','i','try']
    if rule in lol_keywords:
        rule='b3s23'
        
        symmetry=random.choice(validsyms)
    else:
        if rule=='ramdom':
            rule='skilife'
            symmetry='random'
        if symmetry=='random':
            symmetry=random.choice(validsyms)
    
    if rule in nicknames:
        rule=nicknames[rule]        

    if symmetry in validsyms:
        response = "Got that, {}. try searching **{}** in **{}**.".format(ctx.author.mention,rule,symmetry)
        await ctx.send(response)
        minsoups=500000

        if rule!='b3s23' :
            minsoups=10000
            #t=

        target=pathapg+rule+'_'+symmetry
        #apgsearching k search v 0 i 1 n minsoups
        argument=' -k search -v 0 -i 1 -n '+str(minsoups)
        result=runcmd(target+argument)
        if result.returncode == 0 :
            url=hauldomain+rule+'/'+symmetry
            await ctx.send(ctx.author.mention+'\n check: '+url)
        else:
            await ctx.send('fail to search. try :```./profile '+rule+' '+symmetry+"```\n"+str(result.stdout))
    else:
        response='{}\n invalid symmetry: *{}*'.format(ctx.author.mention,symmetry)
        await ctx.send(response)


@bot.command(name='show', help="use './show symmetry' to get valid symmetries")
async def showinfo(ctx,request:to_lower='symmetry'):
    response='Hi! what info do you need?\n "{}" ?  interesting.'.format(request)
    if request=='symmetry':
        response='I have valid symmetries as follows: \n```{}```'.format(str(validsyms))
    elif request=='nicknames':
        response='currently nicknames: \n```{}```'.format(str(nicknames))
    elif request=='primes':
        response='prime numbers: \n```{}```'.format(str(primes))
    await ctx.send(response)



@bot.command(name='profile', help='--recompile apgluxe with --profile flag')    
async def profile(ctx,rule:to_lower='b3-ks23-i',symmetry='C1'):
    if ';' in rule :
        rule='skilife'
    elif '/'in rule:
        rule=rule.replace('/','')
    
    if symmetry in validsyms:
        target='. '+pathapg+'recompile.sh '
        #argument=" --profile  --rule  {0}  --symmetry  {1}  ; mv {2}apgluxe  {2}{0}_{1} ".format(rule,symmetry,pathapg)
        argument=" --profile  --rule  {0}  --symmetry  {1}".format(rule,symmetry)
        result=os.system(target+argument)
        if result == 0 :
            response="{0}\n successfully compile {1} in {2} with profile flag.".format(ctx.author.mention,rule,symmetry)
        else:
            response="cd apgmera/ \n ./recompile.sh --rule {1} --symmetry {2} \n mv apgluxe {1}_{2} \n".format(ctx.author.mention,rule,symmetry)
            #print(result.stderr)
            #print(result.stdout)

    else:
        response='{}\n validsyms needed'.format(ctx.author.mention)
    await ctx.send(response)


@bot.command(name='roll_dice', help='Simulates random dice rolling. try with: \'./roll_dice 1 6\'')
async def roll(ctx, number_of_dice: int=1, number_of_sides: int=6):
    if number_of_sides >=2 and number_of_dice >= 1 :
        dice = [
            str(random.choice(range(1, number_of_sides + 1)))
            for _ in range(number_of_dice)
        ]
        msg=', '.join(dice)
    else:
        msg="how could I roll a *{}* side of dice for *{}* times?".format(number_of_sides,number_of_dice)
    await ctx.send(msg)

bot.run(TOKEN)
